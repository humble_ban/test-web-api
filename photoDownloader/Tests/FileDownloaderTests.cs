using System.IO;
using Common;
using NUnit.Framework;

namespace Tests
{
    public class Tests
    {
        [Test]
        public void FileDownload()
        {
            FileDownloader fd = new FileDownloader();
            var filename = fd.TryDownload("http://900igr.net/up/datas/263154/063.jpg");
            Assert.True(File.Exists(filename));
        }
        
        [Test]
        public void FileNotDownload()
        {
            FileDownloader fd = new FileDownloader();
            var filename = fd.TryDownload("пасхалке №2");
            Assert.True(!File.Exists(filename));
        }
    }
}