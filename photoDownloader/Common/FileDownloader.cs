﻿using System;
using System.ComponentModel;
using System.IO;
using System.Net;
using SixLabors.ImageSharp;
using SixLabors.ImageSharp.Formats;
using SixLabors.ImageSharp.Processing;

namespace Common
{
    public class FileDownloader
    {
        public string TryDownload(string fileUrl)
        {
            try
            {
                using (WebClient wc = new WebClient())
                {
                    var fileName = Path.Combine(Path.GetTempPath(), Guid.NewGuid().ToString("N") + Path.GetExtension(fileUrl));
                    wc.DownloadFileCompleted += wc_DownloadFileCompleted;
                    wc.QueryString.Add("file", fileName);
                    wc.DownloadFileAsync(new Uri(fileUrl), fileName);
                    return fileName;
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                return e.Message;
            }
        }

        void wc_DownloadFileCompleted(object sender, AsyncCompletedEventArgs e)
        {
            if (e.Error != null)
                Console.Write(e.Error.Message);
            else
            {
                try
                {
                    string fileIdentifier;
                    fileIdentifier = ((WebClient) (sender)).QueryString["file"];
                    //после загрузки создать превью
                        var previewPath = Path.Combine(Path.GetTempPath(), Path.GetFileNameWithoutExtension(fileIdentifier) + "_preview") + Path.GetExtension(fileIdentifier);
                        using (var inStream = File.OpenRead(fileIdentifier))
                        using (var outStream = File.Create(previewPath))
                        using (var image = Image.Load(inStream, out IImageFormat format))
                        {
                            var clone = image.Clone(
                                i => i.Resize(100, 100));

                            clone.Save(outStream, format);
                        }
                }
                catch (Exception exception)
                {
                    Console.WriteLine(exception);
                }
            }
        }
    }
}