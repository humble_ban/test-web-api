using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Common;

namespace photoDownloader.Controllers
{
    [Route("image")]
    [ApiController]
    public class Image : ControllerBase
    {
        [Route("index")]
        [HttpGet]
        public ActionResult<string> Index()
        {
            return "работает!";
        }

        [HttpPost]
        [Route("download-list")]
        public IActionResult DownloadList([FromBody] string[] urls)
        {
            Parallel.ForEach(urls, DownloadFile);
            return Ok(urls);
        }

        [HttpPost]
        [Route("download")]
        public IActionResult Download([FromBody] string url)
        {
            DownloadFile(url);
            return Ok(url);
        }

        private void DownloadFile(string url)
        {
            var fileDowloader = new FileDownloader();
            fileDowloader.TryDownload(url);
        }
    }
}